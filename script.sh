#!/bin/bash
set -o errtrace 
echo "Welcome to the CSI stress test tool!"
echo "Please select the context to run this automation:"
PS3='Please enter your choice:'
select context in $(kubectl config get-contexts -o name)
    do
        kubectl config use-context $context
        break;
    done
if [ -z "$context" ]; then echo "Could not find context configured or kubectl is not installed" ;exit 1; fi
echo "#############################################"

echo "Please select the action that you wish to perform"
PS3='Please enter your choice:'
options=("Deploy new mysql deployment in bulk" "Delete bulk deployment" "Run IO Test on a pod deployment" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Deploy new mysql deployment in bulk")
        echo "#############################################"
        read -p "enter the number of deployments that you wnat to create: " number
	    read -p "enter deployment file name: " deployment
         if [ -f "$deployment.yaml" ];
         then
            echo "File $deployment.yaml is exist, don't use the same file name until you delete the old deployment";
            break;
         fi
	    touch ./$deployment.yaml
            for((i=1;i <= number; i++))
            do
              SRVID=$(cat /dev/urandom | env LC_ALL=C tr -dc 'a-z' | fold -w 10 | head -n 1)
              cp mysqlimage.yaml "$SRVID.yaml"
              sed -i "s/testapp-mysql/$SRVID/g" "$SRVID.yaml"
              cat $SRVID.yaml >> $deployment.yaml
              rm "$SRVID.yaml"
            done
            kubectl apply -f $deployment.yaml
	        break
            ;;
        "Delete bulk deployment")
            echo "#############################################"
            PS3="Pick a file number with the deployment that you like to remove (or Ctrl-C to quit): "
            select f in *.yaml
            do
                kubectl delete -f $f;
                rm $f;
                break; 
            done
            break
            ;;
        "Run IO Test on a pod deployment")
            echo "#############################################"
            PS3="Pick one of the pods that deployed via the script: "
            select pod in $(kubectl get pods -l app=testapp -o jsonpath="{.items[*].metadata.name}")
            do
                echo $pod;
                PASS="P@ssw0rd";
                read -p "Enter the number of Iterations that you want to create on the pod: " iter
                read -p "Number of client sessions to run: " client
                kubectl exec $pod -- mysqlslap --concurrency=$client --iterations=$iter --number-int-cols=10 --number-char-cols=10 --auto-generate-sql -u root -p$PASS;
                break;
            done
            if [ -z "$pod" ]; then echo "Could not find pods in k8s cluster, please make sure that you run the first command and creating deployment before running this" ; fi
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
